#include "BSP.h"
#include "mg_api.h"
#define APPLICATION_ADDRESS     (uint32_t)0x8001010
#define FLASH_E2PROM_ADDR_BASE (0x08000000)
#define FLASH_BOOT_ROM_SIZE    (4*1024)
#define FLASH_E2PROM_ADDR_USR  (FLASH_E2PROM_ADDR_BASE + 4*1024)
#define FLASH_E2PROM_ADDR_OTA  (FLASH_E2PROM_ADDR_BASE + 64*1024)
#define FLASH_E2PROM_ADDR_OTA_KB (32)

extern unsigned int SysTick_Count;
extern unsigned int RxTimeout;
void CodeNvcRemap(void);

void CodeNvcRemap(void)
{
    uint32_t i = 0;
    for(i = 0; i < 48; i++) {
        *((uint32_t*)(0x20000000 +  (i << 2))) = *(__IO uint32_t*)(APPLICATION_ADDRESS + (i << 2));
    }
    RCC->APB2ENR |= 0x00000001;
    SYSCFG->CFGR |= 0x03;
}


u32 GetOtaAddr(void)
{
    return FLASH_E2PROM_ADDR_OTA;
}

u32 GetCodeAddr(void)
{
    return (FLASH_E2PROM_ADDR_BASE + FLASH_BOOT_ROM_SIZE);
}

void WriteFlashE2PROM(u8* data, u16 len, u32 pos, u8 flag) //4 bytes aligned
{
    u32 t;

    if(flag)FLASH_Unlock();
    while(len >= 4) {
        t = data[3]; t <<= 8;
        t |= data[2]; t <<= 8;
        t |= data[1]; t <<= 8;
        t |= data[0];
        FLASH_ProgramWord(pos, t);
        pos += 4;
        len -= 4;
        data += 4;
    }

    if(flag)FLASH_Lock();
}

void OtaSystemReboot(void)//porting api
{
    NVIC_SystemReset();
}
