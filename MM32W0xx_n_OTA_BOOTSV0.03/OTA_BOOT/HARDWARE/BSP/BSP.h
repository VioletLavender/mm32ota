#ifndef _BSP_H_
#define _BSP_H_

#include "HAL_conf.h"
#include "spi.h"

unsigned GetIRQSta(void);
void CodeNvcRemap(void);
void ChangeBaudRate(void);


#endif
