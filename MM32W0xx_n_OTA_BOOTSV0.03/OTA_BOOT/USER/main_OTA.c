#include <string.h>
#include <stdlib.h>
#include "HAL_conf.h"
#include "BSP.h"
#include "mg_api.h"
#include "irq_rf.h"
#include "HAL_device.h"
#include "rcc.h"
#include "sys.h"
#include "app.h"
#include "spi.h"

extern volatile unsigned int SysTick_Count;

unsigned char *ble_mac_addr;
unsigned char* get_local_addr(void) //used for ble pairing case
{
    return ble_mac_addr;
}
const unsigned char kkkk[28000] = {
    0x02, 0x01, 0x06,
    0x03, 0x03, 0x90, 0xfe
};

const unsigned char OTAAdvDat[] = {
    0x02, 0x01, 0x06,
    0x03, 0x03, 0x90, 0xfe
};
unsigned char OTARspDat[] = {
    0x4, 0x09, 'O', 'T', 'A',
    0x05, 0xff, 0xcd, 0xab, 0x23, 0x56
};
//static void delayloop(u32 delay);

int main(void)
{
    int i;
    unsigned long temp = 0x180000;
    if(kkkk[19000]==0)
    {
        temp = 0x180000;
    }
    else{
        temp = 0x180001;
    }
    while(temp--);//wait a while for hex programming if using the MCU stop mode, default NOT used.
		CodeNvcRemap();
    SystemClk_HSEInit();
    SysTick_Configuration();
    SPIM_Init(SPI2,/*0x06*/0x08); //6Mhz
		IRQ_RF();
//    for(i = 0; i < 23000; i++)
//    {
//        temp+=kkkk[i];
//    }
//        if(temp==0)
//    {
//        for(i = 0; i < 5; i++) {  delayloop(300); delayloop(300);}
//    }
//    else{
//        for(i = 0; i < 6; i++) { delayloop(300);  delayloop(300);}
//    }
    SetBleIntRunningMode();
    radio_initBle(TXPWR_0DBM, &ble_mac_addr);
    ble_set_adv_data((unsigned char *)OTAAdvDat, sizeof(OTAAdvDat));
    ble_set_adv_rsp_data((unsigned char *)OTARspDat, sizeof(OTARspDat));

    SysTick_Count = 0;
    while(SysTick_Count < 5){}; //delay at least 5ms between radio_initBle() and ble_run...

    ble_run_interrupt_start(160*2); //320*0.625=200 ms
    
    while(1){
       IrqMcuGotoSleepAndWakeup();
        
    }
}
//static void delayloop(u32 delay)
//{

//    u32 i, j;
//    for(i = 0; i < delay; i++) {
//        for(j = 0; j < 10000; j++) {
//            __NOP();
//        }
//    }
//}
